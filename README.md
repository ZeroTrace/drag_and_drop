# Prueba drag_and_drop

El proyecto tiene un boton para iniciar sesión, cuando le das click, tiene una animación que te lleva al juego.

Obtiene los datos de los perros de manera aleatoria de la <a href="https://dog.ceo/dog-api/documentation/random">API de Dogs</a>
una vez que obtiene los datos, itera el arreglo y saca el nombre de cada perro, cambia los "-" por espacios y pone en mayúscula 
la primera letra.

Llena las imágenes de los perros, aleatoriza el arreglo de los perros para acomodar los nombres de manera diferente.

Cuando agrega los elementos le agrega un elemento data-response, que contiene el nombre de la raza que es utilizado para comparar si la
respuesta es correcta.

En caso de que la respuesta no sea correcta, se reinicia el juego con los mismos perros y se vuelven a mezclar las respuestas.

Cada respuesta que se hace correctamente, se pone de color verde.

Al completar las 3 respuestas correctas, se activa la opción para poder abrir el modal y se activan los botones para
iniciar un nuevo juego(traer nuevos datos de la API) o reiniciar el mismo.

Se utilizaron las siguientes librerias:

* <a href="">jQuery</a>
* <a href="">jQueryUI</a>
* <a href="https://github.com/daneden/animate.css">animate.css</a> 

Se utilizó la API: 
* <a href="https://dog.ceo/dog-api/documentation/random">Dog API</a>

Se utilzaron imágenes aleatorias de unsplash
* <a href="https://source.unsplash.com/1920x1080/?gameboy-color">Unsplash gameboy images</a>

Para el background se usó un gradiente obtenido de:
* <a href="https://cssgradient.io/gradient-backgrounds/">cssgradient.io</a>