// Se almacenan los datos obtenidos de la API para poder reiniciar el juego sin tener que consultarla de nuevo
let dogs = [];
let gameCompleted = false;
let completed = 0;

Array.prototype.shuffle = function () {
    let input = this;

    for (let i = input.length - 1; i >= 0; i--) {

        let randomIndex = Math.floor(Math.random() * (i + 1));
        let itemAtIndex = input[randomIndex];

        input[randomIndex] = input[i];
        input[i] = itemAtIndex;
    }
    return input;
};

// Limpia los contenedores de las imagenes y las razas
function clearContainers() {
    $('#modal').addClass('hidden');
    $('.dog-image').addClass('disabled');
    $('.buttons-restart button').addClass('hidden');
    completed = 0;
    gameCompleted = false;
    $("#dog-images").empty();
    $('#dog-names').empty();
}

function closeModal() {
    $('#modal').addClass('hidden');
}

// Inicializa el juego al darle click al botón "Iniciar"
function startGame() {
    let start = document.querySelector('#inicio');
    let game = document.querySelector('#juego');

    if (start.classList.contains('fadeInLeft')) {
        start.classList.replace('fadeInLeft', 'fadeOutLeft');
        game.classList.replace('fadeOutRight', 'fadeInRight');
    } else {
        start.classList.add('fadeOutLeft');
        game.classList.add('fadeInRight');
    }

    fetchDogs().then(response => {
        dogs = response.message;
        fillDogContainers(dogs)
    });
}

// Reinicia el juego al darle click al boton "Reiniciar"
function restartGame() {
    fillDogContainers(dogs);
}

// Obtiene el JSON desde la API
function fetchDogs() {
    let fetchRandomDogsURL = "https://dog.ceo/api/breeds/image/random/3";
    return $.get(fetchRandomDogsURL);
}

// Limpia los contenedores y los llena con la información obtenida del json
function fillDogContainers(dogs) {
    clearContainers();
    let dogImagesContainer = $("#dog-images");
    let dogNamesContainer = $('#dog-names');

    let mappedDogs = dogs.map(dog => {
        return {
            dogName: dog.split('/')[4].replace("-", " "),
            dogImage: dog
        };
    });

    // Agrega las imágenes
    for (let i = 0; i < dogs.length; i++) {
        // Agrega la imagen del perro
        dogImagesContainer.append(`
            <div class="dog-image__container">
                <img class="dog-image disabled" src="${mappedDogs[i].dogImage}" alt="Perro ${i + 1}" onclick="showModal('${mappedDogs[i].dogImage}', '${mappedDogs[i].dogName}')"/>
                <div class="name-response" data-response="${mappedDogs[i].dogName}"></div>
            </div>
        `);
    }

    mappedDogs.shuffle();
    for (let i = 0; i < dogs.length; i++) {
        // Agrega el nombre de la raza
        dogNamesContainer.append(`
            <div class="dog-name__container" data-response="${mappedDogs[i].dogName}">
                <span class="dog-name">${mappedDogs[i].dogName}</span>
            </div>
        `);
    }


    // Hace que los nombres de las razas sean arrastrables
    $('.dog-name__container').draggable();

    // Hace que los contenedores abajo de las fotos de los perros soporten el evento de drop
    $('.name-response').droppable({
        drop: function (event, ui) {
            // Si el la raza del perro y la imagen coinciden, deshabilita el drag y el drop
            if (event.target.dataset.response === ui.draggable[0].dataset.response) {
                ui.draggable[0].classList.add('correct');
                $(event.target).droppable('disable');
                $(ui.draggable[0]).draggable('disable');
                completed++;

                if (completed === 3) {
                    $('.buttons-restart button').removeClass('hidden');
                    gameCompleted = true;
                    $('.dog-image').removeClass('disabled');
                }
            } else {
                // Si te equivocas se reinicia el juego
                restartGame();
            }
        }
    });
}

function showModal(dogImage, dogName) {
    $('#dog-detail').attr('src', dogImage);
    $('#dog-name__modal').text(dogName);
    $('#modal').removeClass('hidden');
}

// Regresa a la pantalla de inicio al darle click a la flecha para atras
function returnToStart() {
    let game = document.querySelector('#juego');
    let start = document.querySelector('#inicio');

    start.classList.replace('fadeOutLeft', 'fadeInLeft');
    game.classList.replace('fadeInRight', 'fadeOutRight');
}